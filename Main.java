package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner num = new Scanner(System.in); // ввод данных
        float multiplier1, multiplier2, first, second, result;

        System.out.println("Приветствуем вас в программе расчета заработной платы работника!");
        System.out.println("Введите количество часов (Помните , что значение не может превышать 60ч):");
        first = num.nextInt();
        System.out.println("Зарплату за час(Помните, что значение не может принижать количиства 8$):");
        second = num.nextInt();

        if (first > 60) { //проверка на то , что работник не работает больше 60 часов


            System.out.println("Вы задали не верное значение проработаных часов или зарплаты работника!");
            return;
        }

        if (second < 8) { //проверка на то, что работник не зарабатывает меньше 8$

            System.out.println("Вы задали не верное значение проработаных часов или зарплаты работника!");
            return;
        }

        if (first >= 40) { //проверка на то, что работник работает больше 40 часов
            multiplier1 = ((first - 40) * 1.5f) + 40 ;
            result = multiplier1 * second; // ответ
            System.out.print("Работник заработал за неделю: " + result + " $ ");
            System.out.println(" ");

        }

        if (first < 40) {
            multiplier2 = first * second;
            result = multiplier2; // ответ
            System.out.print("Работник заработал за неделю:" + result + " $ ");
            System.out.println(" ");
        }


    }
}